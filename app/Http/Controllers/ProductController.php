<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;
use App\Cart;
use Validator;

class ProductController extends Controller
{
    public function getProducts()
    {
    	$products =Product::all();
    	return view('shop.products',['products'=>$products]);
    }
    public function addProduct()
    {
      return view('shop.add-product');
    }
    public function postProduct(Request $request)
    {
        $this->validate($request,[
          'title'=>'required',
          'description'=>'required',
          'price'=>'required',
          'imagePath'=>'image|mimes:png,jpg,jpeg|max:10000',
          ]);
        //image upload
        $image = $request->imagePath;
        if( $image){
          $imageName =$image->getClientOriginalName();
          $image->move('images/shop',$imageName);
          $formInput['image']=$imageName;
        }

        $product = new Product([
        'title'=>$request->input('title'),
        'description'=>$request->input('description'),
        'price'=>$request->input('price'),
        'imagepath'=>$request->input('image'),
        ]);
      $product->save();
      return view('add.product');


    }
    public function getAddToCart(Request $request, $id)
   {
      $product = Product::find($id);
      $oldCart = Session::has('cart')? Session::get('cart') : null;
      $cart = new Cart($oldCart);
      $cart->add($product,$product->id);
      $request->session()->put('cart', $cart);
      return redirect(route('product.index'));
   }
   public function getCart()
   {
    if(!Session::has('cart')){
      return view('shop.shopping-cart');
    }
    $oldCart = Session::get('cart');
    $cart = new Cart($oldCart);
    return view ('shop.shopping-cart',['products'=>$cart->items,'totalPrice'=>$cart->totalPrice]);
   }
   public function getCheckout()
   {
      if(!Session::has('cart')){
        return view('shop.shopping-cart');
      }
      $oldCart = Session::get('cart');
      $cart = new Cart( $oldCart);
      $total = $cart->totalPrice;
      return view('shop.checkout',['total'=>$total]);

   }
   public function postCheckout(){

   }
}
