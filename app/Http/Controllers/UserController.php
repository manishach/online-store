<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    public function index()
   {
      return view('shop.index');
   }
   public function getSignup()
   {
   	return view('user.register');
   }
   public function postSignup(Request $request)
   {
   		$this->validate($request, [
   			'email'=>'email|required|unique:users',
   			'password'=>'required|min:4'
   			]);
   		$user = new User([
   			'email'=>$request->input('email'),
   			'password'=>bcrypt($request->input('password'))
   			]);
   		$user->save();
         return redirect(route('user.signup'));
   }
   public function getsignin()
   {
      return view('user.login');
   }
   public function postsignin(Request $request)
   {
      $this->validate($request, [
            'email'=>'email|required',
            'password'=>'required|min:4'
            ]);
      if(Auth::attempt(['email'=> $request->input('email'),'password'=>$request->input('password')]))
      {
         return redirect(route('user.profile'));
      }
      return redirect()->back();
   }
   public function getprofile()
   {
      return view('user.profile');
   }
   public function getLogout()
   {
      Auth::logout();
      return redirect()->back();
   }
}
