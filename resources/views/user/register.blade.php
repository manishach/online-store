@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
      <h1>Sign Up</h1> 
      @if(count($errors) > 0)  
      <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <p>{{$error}}</p>
        @endforeach
      </div>
      @endif   
        <form action="{{route('user.signup')}}" method="post">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-primary">SignUp</button>
        </form>
    </div>
  </div>
</div>
@endsection