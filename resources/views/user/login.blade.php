@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
      <h1>Sign In</h1>      
        <form action="{{route('user.signin')}}" method="post">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-primary">Sign In</button>
        </form>
    </div>
  </div>
</div>
@endsection