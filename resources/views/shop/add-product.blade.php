@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <form action="{{route('add.product')}}" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
	            <div class="form-group">
			    	<label for="image">Upload Image</label>
   					 <input type="file" id="image" name="image">
				  </div>
				  <div class="form-group">
				    <label >Title</label>
				    <input type="text" class="form-control" id="title" placeholder="Title">
				  </div>
				  <div class="form-group">
				    <label >Description</label>
				    <input type="text" class="form-control" id="description" placeholder="Description">
				  </div>
				  <div class="form-group">
				    <label>price</label>
				    <input type="text" class="form-control" id="price" placeholder="Price">
				  </div>
				  
				  <button type="submit" class="btn btn-primary">Add Product</button>
				</form>
            </div>
          </div>  
	</div>
</div>
@endsection