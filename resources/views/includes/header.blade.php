<header id="header"><!--header-->		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4" style="margin-top: -16px;">
						<div class="logo pull-left">
							<!--<a href="#"><img style="width: 135px;" src="images/shop/logo1.png" alt="" /></a>-->
							<h1><span style="color:#FE980F;">ONLINE </span>SHOPPING</h1>
						</div>						
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li>
									<a href="{{ route('product.shoppingCart')}}">
										<i class="fa fa-shopping-cart"></i> Cart
										<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
									</a>
								</li>
								 <li class="dropdown">
							          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>Account <span class="caret"></span></a>
							          <ul class="dropdown-menu">
							            <li><a href="{{route('user.signin')}}"><i class="fa fa-lock"></i> Login</a></li>
							            <li><a href="{{route('user.signup')}}"><i class="fa fa-lock"></i>Register</a></li>
							            <li><a href="{{route('user.logout')}}"><i ></i> Logout</a></li>
							          </ul>
							        </li>
							</ul>
						</div>
						<div style="clear:both;">							
						</div>
					</div>

				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="{{route('product.index')}}" class="active">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="{{ url('/products') }}">Products</a></li>
                                    </ul>
                                </li> 
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	