<?php



Route::get('/',[
	'as'=>'product.index',
	'uses'=>'UserController@index'
	]);

Route::get('/add-product',[
	'as'=>'add.product',
	'uses'=>'ProductController@addProduct'
	]);
Route::post('/add-product',[
	'as'=>'add.product',
	'uses'=>'ProductController@postProduct'
	]);
Route::get('/add-to-cart/{id}',[
	'as'=>'product.addToCart',
	'uses'=>'ProductController@getAddToCart'
	]);


Route::get('/shopping-cart',[
	'as'=>'product.shoppingCart',
	'uses'=>'ProductController@getCart'
	]);

Route::get('/checkout',[
	'as'=>'checkout',
	'uses'=>'ProductController@getCheckout'
	
	]);
Route::post('/checkout',[
	'as'=>'checkout',
	'uses'=>'ProductController@postCheckout'
	]);

Route::get('/products','ProductController@getProducts');

Route::group(['prefix'=>'user'], function(){
	

	
	Route::get('/signup',['as'=>'user.signup','uses'=>'UserController@getSignup'] );
	Route::post('signup',['as'=>'user.signup','uses'=>'UserController@postSignup']);

	Route::get('signin',['as'=>'user.signin','uses'=>'UserController@getsignin'] );
	Route::post('signin',['as'=>'user.signin','uses'=>'UserController@postsignin']);

	Route::get('profile',['as'=>'user.profile','uses'=>'UserController@getprofile'] );
	Route::get('/logout',['as'=>'user.logout','uses'=>'UserController@getLogout'] );
});

